Facter.add(:restic_backups) do
  confine kernel: 'Linux'
  setcode do
    restic_backups = {}
    bin_grep = ''
    dir_crontab = ''

    osfamily = Facter.value(:osfamily)
    case osfamily
    when 'RedHat'
      bin_grep = '/usr/bin/grep'
      dir_crontab = '/var/spool/cron'
    when 'Debian'
      bin_grep = '/bin/grep'
      dir_crontab = '/var/spool/cron/crontabs'
    end

    if File.exist? bin_grep.to_s
      begin
        backupnames = []

        crontab_backup_list = Facter::Core::Execution.execute("#{bin_grep} -HP '^#.+restic-backup-.*' #{dir_crontab}/*", timeout: 2)
        crontab_backup_list.split(%r{\n})[0..-1].map do |crontab_line|
          username, backupname = crontab_line.match(%r{^#{dir_crontab}/([^:]+):#.+\s(restic-backup-.*)}).captures

          if restic_backups[username].nil?
            backupnames = []
          end

          backupnames = backupnames.concat([backupname.to_s])
          restic_backups[username] = backupnames
        end
      rescue Facter::Core::Execution::ExecutionFailure
        restic_backups['timeout'] = "#{bin_grep} timedout"
      end
    else
      restic_backups['error'] = "file not found : #{bin_grep}"
    end

    restic_backups
  end
end
