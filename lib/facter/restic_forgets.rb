Facter.add(:restic_forgets) do
  confine kernel: 'Linux'
  setcode do
    restic_forgets = {}
    bin_grep = ''
    dir_crontab = ''

    osfamily = Facter.value(:osfamily)
    case osfamily
    when 'RedHat'
      bin_grep = '/usr/bin/grep'
      dir_crontab = '/var/spool/cron'
    when 'Debian'
      bin_grep = '/bin/grep'
      dir_crontab = '/var/spool/cron/crontabs'
    end

    if File.exist? bin_grep.to_s
      begin
        forgetnames = []

        crontab_forget_list = Facter::Core::Execution.execute("#{bin_grep} -HP '^#.+restic-forget-.*' #{dir_crontab}/*", timeout: 2)
        crontab_forget_list.split(%r{\n})[0..-1].map do |crontab_line|
          username, forgetname = crontab_line.match(%r{^#{dir_crontab}/([^:]+):#.+\s(restic-forget-.*)}).captures

          if restic_forgets[username].nil?
            forgetnames = []
          end

          forgetnames = forgetnames.concat([forgetname.to_s])
          restic_forgets[username] = forgetnames
        end
      rescue Facter::Core::Execution::ExecutionFailure
        restic_forgets['timeout'] = "#{bin_grep} timedout"
      end
    else
      restic_forgets['error'] = "file not found : #{bin_grep}"
    end

    restic_forgets
  end
end
