require 'fileutils'
require 'etc'

Puppet::Type.type(:restic_init).provide(:restic) do
  desc 'restic provider for restic_init resource'

  confine kernel: :linux
  defaultfor kernel: :linux

  def environment
    env = {}
    env['RESTIC_PASSWORD'] = resource[:password]
    env['RESTIC_PASSWORD2'] = resource[:password2] if resource[:password2]
    env
  end

  def run_restic(args)
    output = Puppet::Util::Execution.execute(
      ['restic', args].join(' '),
      failonfail: false,
      combine: true,
      uid: Etc.getpwnam(resource[:user].to_s).uid,
      custom_environment: environment,
    )

    raise ArgumentError, output if output.exitstatus == 127
  end

  def create
    if resource[:repo2]
      run_restic(['init', '--repo', resource[:path], '--repo2', resource[:repo2], '--copy-chunker-params'])
    else
      run_restic(['init', '--repo', resource[:path]])
    end
  end

  def destroy
    FileUtils.rm_rf resource[:path] if File.file?("#{resource[:path]}/config")
    raise("Path #{resource[:path]} does not look to be a Restic repository")
  end

  def exists?
    File.file?("#{resource[:path]}/config")
  end
end
