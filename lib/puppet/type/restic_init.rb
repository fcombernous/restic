Puppet::Type.newtype(:restic_init) do
  @doc = 'Creates and removes a local Restic repository'

  ensurable

  validate do
    raise('password is required when ensure is present') if self[:ensure] == :present && self[:password].nil?
    raise('password2 is required when ensure is present and repo2 is used') if self[:ensure] == :present && self[:password2].nil? && !self[:repo2].nil?
  end

  newparam(:path) do
    desc 'The local path of repository'

    validate do |value|
      unless Pathname.new(value).absolute?
        raise("Absolute path required. Invalid path value : #{value}")
      end
    end

    isnamevar
  end

  newparam(:password) do
    desc 'The password of repository'

    validate do |value|
      if value.nil? || value.empty?
        raise("Invalid password value #{value}")
      end
    end
  end

  newparam(:user) do
    desc 'The user owner of repository, default to root'

    defaultto :root
  end

  newparam(:repo2) do
    desc 'Already existing repository used to get chunk parameters to init a copy repository given in path parameter'

    validate do |value|
      if value.nil? || value.empty?
        raise("Invalid repo2 value #{value}")
      end
    end
  end

  newparam(:password2) do
    desc 'The password of already existing repository used to copy chunker parameter for this new copy repository'

    validate do |value|
      if value.nil? || value.empty?
        raise("Invalid password value #{value}")
      end
    end
  end
end
