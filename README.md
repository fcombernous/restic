# restic

#### Table of Contents

1. [Description](#description)
1. [Setup - The basics of getting started with restic](#setup)
    * [What restic affects](#what-restic-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with restic](#beginning-with-restic)
1. [Usage - Configuration options and additional functionality](#usage)
1. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
1. [Limitations - OS compatibility, etc.](#limitations)
1. [Development - Guide for contributing to the module](#development)

## Description

[Restic](https://restic.net/) is a program that does backups. This module install Restic, configure a repository, manage backups frequencies and setup data deletion policies.

## Setup

### What restic affects

What the module does:

 * Install Restic.

 * Use cron tasks to manage backups and data deletion in repository (Restic does not launch any service. `restic` commands are planned with cron service). 

What the module does not do:

 * Create a user account.

### Setup Requirements

This module is tested with dependencies and Puppet versions listed as supported by [metadata.json](metadata.json).

### Beginning with restic

To have Restic installed, declare the `restic` class:

```
include restic
```

By default the backups you will define will be done as root. You can use restic's ability to make a backup without root privileges.
For instance, with an already existing user account named `foo`, you can declare this :

```
class { 'restic':
  runasuser    => 'foo',
  wrapper_path => '/opt/restic',
}
```
Default path used by `restic::wrapper_path` is root only access. And so when using non root users it is also necessary to define a `wrapper_path`.

## Usage

### Create local repository

The place where your backups will be saved at is called a *repository*. Restic can handle several kinds of repositories. For the moment, the restic provider handle only local repository. Updating parameters `password` or `runasuser` does not update repository already created.

To configure a local repository owned by an already existing account `foo` :

```
class { 'restic::initrepo' :
  runasuser           => 'foo',
  repository_url      => '/myrepository',
  repository_password => 'repopassword',
}
```

To install Restic as `foo`, configure a repository and setup a forget policy named `global` :

```
class { 'restic' :
  runasuser           => 'foo',
  repository_url      => '/myrepository',
  repository_password => 'repopassword',
  forgets => {
    global =>  {
      cron_frequency      => 'weekly',
      keeps               => {
        'daily'  => 14,
        'weekly' => 3,
      },
    },
  },
}

To install Restic as `bar`, configure a copy repository with same chunker settings than
repository `foo@node1:/myrepository` and setup a forget policy named `global` :

```
class { 'restic' :
  runasuser            => 'bar',
  repository_url       => '/myrepository_copy',
  repository_password  => 'repopasswordcopy',
  repository_url2      => 'foo@node1:/myrepository',
  repository_password2 => 'repopassword',
  forgets => {
    global =>  {
      cron_frequency      => 'weekly',
      keeps               => {
        'daily'  => 14,
        'weekly' => 3,
      },
    },
  },
}


The key word `global` permits to use `restic forget` without targeting snapshots of a specific hostname. If you don't use `global` as key of hash, the `restic forget` command will use the key name of the hash as hostname with flag `--hostname`.

```

### Configure several backups of a node

```
class { 'restic': 
  runasuser => 'foo',
  backups   => {
    'dbdumps' => {
      repository_url      => 'sftp:bar@resticserver:/var/resticrepo',
      repository_password => 'barpass',
      cron_frequency      => 'daily',
      inodes              => ['/var/dbdumps'],
    },
    'wwwstaticdata' => {
      repository_url      => 'sftp:bar@resticserver:/var/resticrepo',
      repository_password => 'barpass',
      cron_frequency      => 'weekly',
      cron_hour           => 4,
      cron_minute         => 42,
      cron_weekday        => '2',
      inodes              => ['/var/www/'],
    },
    'entirehost' => {
      repository_url      => 'sftp:bar@resticserver:/var/resticrepo',
      repository_password => 'barpass',
      cron_frequency      => 'monthly',
      cron_hour           => 4,
      cron_minute         => 42,
      cron_monthday       => '3',
      excludes            => ['/var/dbdumps/','/dev','/proc','/sys','/mnt','/media'],
    },
  }
}
```

## Reference

Details in [REFERENCE.md](REFERENCE.md).

## Limitations

This module is tested on all OSes listed as supported by [metadata.json](metadata.json).

## Development

Contribs are welcome. Please read [CONTRIBUTING.md](CONTRIBUTING.md) before.

Module under AGPL v3, with a copy in [LICENSE](LICENSE) file.

Module hosted at https://gitlab.adullact.net/fcombernous/restic

## Release Notes/Contributors/Etc.

See details in [CHANGELOG](CHANGELOG).

Author : Fabien Combernous.

