# @summary define a restic repository
type Restic::Repositoryurl = Variant[
  Stdlib::Unixpath,
  Pattern['^sftp'],
]
