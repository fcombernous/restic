# @summary define a repository
type Restic::Repository = Struct[{
  repository_url      => Restic::Repositoryurl,
  repository_password => String[1],
}]
