# @summary define what a forget keeps in restic repository
type Restic::Forgetkeeps = Struct[{
  Optional[last]    => Integer,
  Optional[hourly]  => Integer,
  Optional[daily]   => Integer,
  Optional[weekly]  => Integer,
  Optional[monthly] => Integer,
  Optional[yearly]  => Integer,
  Optional[tag]     => String,
}]
