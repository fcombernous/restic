Release 8.0.0 :

* allow puppetlabs-stdlib >= 9.0.0 < 10.0.0, Drop Puppet 6, Drop CentOS7, Drop Debian 11, Add Debian 12 #112
* use PDK 3.2.0 and Ruby 3.2.0 #111

Release 7.2.0 :

 * search target server into known_hosts #108

Release 7.1.0 :

 * Add parameter cache_dir to set a custom cache directory #106

Release 7.0.0 :

 * default to restic version 0.13.0 #104
 * add debian11, ubuntu2204 and drop debian10, ubuntu1604 #103
 * use pdk 2.5.0 and ruby 2.7.5 #102

Release 6.0.0 :

 * pdk maintenance 1.18.1 #99
 * allow puppetlabs-stdlib < 9 , puppetlabs-apt < 9 , puppet-archive < 7 #100
 * drop Puppet 5, add Puppet 7 #98

Release 5.0.0 :

 * doc : inform that limits are during restic copy command #96
 * clean up usage of binary_path parameter #94
 * do not try to create remote repository used as a copy #95

Release 4.0.0 :

 * create a type/provider for "restic init" sub command #75
 * add feature to create copy repository #88
 * add feature restic copy #89
 * default restic version to 0.10.0 #90
 * drop debian-9, add debian-10 , add ubuntu-20.04 #92

Release 3.1.1 :

 * Extend allowed returned value with ssh command not with local repository #86

Release 3.1.0 :

 * Add parameter to permit more than 0 as allowed returned value with ssh command #84

Release 3.0.0 :

 * Doc : fix some data with unused custom data type #83
 * Restic default version to v0.9.6 #81
 * Set default value for repository in defined resources backup and forget #80
 * Doc : add forget example #79
 * Doc : update README without Forgetat and Backupat usage #78
 * Remove custom data type Restic::Backupat for attributes cron_* #77
 * Simplify forget and remote custom data type Restic::Forgatat for attributes cron_* #76
 * Doc : set restic binary path #74
 * Handle several repositories on a same host #73
 * Modify password management #17

Release 2.2.1 :

 * hard link to binary need to be updated when version is modified #71

Release 2.2.0 :

 * downloaded_checksum must be parameter #69

Release 2.1.0 :

 * bzip2 can be managed somewhere else #67
 * default value for restic::wrapper_path is root only readable #65

Release 2.0.0 :

 * install Restic binary provided by Restic project instead of packages (#54, #59)
 * do not call forgets or backups classes when corresponding hash is empty (#55)
 * define wrapper path as parameter (#60)
 * add logdir parameter for logfiles of cron jobs (#61)
 * reduce inheritence (#62)
 * add centos 7 support (#57)
 * add debian 9 support (#58, #64)

Release 1.1.0 :

 * check-ssh-connection connect as root (#52)

Release 1.0.0 :

 * Move some data and logic out of params.pp (#50)
 * Use links in README.md (#49)
 * Add Puppet6 (#48)
 * Allow puppetlabs/apt 7.x, puppetlabs/stdlib 6.x, drop Puppet4, update pdk (#47)
 * Fix README typos (#46)
 * Fix limitations in README.md (#45)
 * Create a datatype from hash globalforget (#44)
 * Remove default_forget_prune from params.pp (#43)
 * Add ubuntu18.04 as supported OS, Drop debian (#42)
 * Add more data types (#41)
 * Add licence and contributing guide (#40)
 * Remove bumpversion (#39)
 * Cleanup some scripts (#37)
 * Use puppet string for docs and REFERENCE.md (#35)
 * Use custom datatypes (#34)
 * Instructs Go's garbage collector to be more aggressive (#33)
 * Use guidance of pdk (#31)

Release 0.13.0 :

 * add monitoring script to check errors in logs (#29)

Release 0.12.0 :

 * fix restic_cron_command stderr ouput redirect (#28)
 * add more checks to data structure (#27)

Release 0.11.0 :

 * Add monitoring checks (#24)
 * add monthly and yearly in default keep (#25)
 * remove duplicated key :restic_forgets in rspec tests about forgets_spec (#26)

Release 0.10.0 :

 * use same naming method for wrappers (#18)
 * Removing snapshots according to a policy not dedicated to a hostname (#22)
 * Fix README bugs (#23)

Release 0.9.0 :

 * fix README for class restic::forgets (#19)
 * fix README to rename hour parameter (#21)
 * fix unless for init repo (#20)

Release 0.8.0 :

 * removing snapshots according to a policy (#13)
 * permit backup absent with minimal parameters (#15)
 * refactoring backup variables names (#16)

Release 0.7.0 :

 * update metadata.json to add compatibility (#9)
 * add ensure attribute to backup defined type (#10)
 * fix README about ensure attribute of defined ressource backup (#11)
 * redirect output of restic wrapper to a logfile (#12)

Release 0.6.0 :

 * add debian jessie as supported OSes (#6)
 * README update section "What restic affects" (#7)
 * remove unused variable default_repository_type (#8)

Release 0.5.0 :

 * eyaml and yaml backend does not looks to manage puppet data type in a same maner(#5)

Release 0.4.0 :

 * fix README usage of class restic::backups (#4)

Release 0.3.0 :

 * refactoring for clearer resources (#3)

Release 0.2.0 :

 * the cron resource does not reset parameters that are removed from a manifest (#1)
 * wrong cron attributs for monthly frequency (#2)
