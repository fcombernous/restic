require 'spec_helper'

describe 'restic' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) do
        os_facts.merge(
          restic_backups: { root: ['foo1'] },
          restic_forgets: { root: ['foo2'] },
        )
      end

      it { is_expected.to compile }

      context 'with repository2 filled but without repository' do
        let(:params) do
          {
            repository2_url: '/var/foo_repo',
            repository2_password: 'secret',
          }
        end

        it { is_expected.to compile.and_raise_error(%r{Missing parameters repository_url and repository_password to prepare copy with chunker settings used in repository2}) }
      end
    end
  end
end
