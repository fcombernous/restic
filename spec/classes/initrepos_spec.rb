require 'spec_helper'

describe 'restic::initrepos' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }
      let(:pre_condition) do
        'include restic'
      end

      let(:params) do
        {
          repositories: [
            {
              repository_url: '/repo_a',
              repository_password: 'pwd_a',
            },
          ],
        }
      end

      it { is_expected.to compile }
    end
  end
end
