require 'spec_helper'

default_runasuser = 'root'
cron_name = 'restic-forget'

describe 'restic::forgets' do
  let(:default_params) do
    {
      runasuser: default_runasuser.to_s,
      forgets: {},
    }
  end
  let(:pre_condition) do
    'include restic'
  end

  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) do
        os_facts.merge(
          restic_backups: { root: ['foo1'] },
          restic_forgets: { root: ['foo2'] },
        )
      end
      let(:params) { default_params }

      it { is_expected.to compile }

      context 'with custom forget parameters' do
        let(:params) do
          default_params.merge(
            forgets: {
              node1: {
                'runasuser'           => default_runasuser.to_s,
                'repository_url'      => '/var/foo',
                'repository_password' => 'foopass',
              },
              node2: {
                'runasuser'           => default_runasuser.to_s,
                'repository_url'      => '/var/foo',
                'repository_password' => 'foopass',
              },
            },
          )
        end

        it { is_expected.to contain_restic__forget('node1') }
        it { is_expected.to contain_restic__forget('node2') }
      end

      context 'with undefined forgets present in facts' do
        let(:facts) do
          os_facts.merge(
            restic_forgets: {
              'root' => ["#{cron_name}-node1", "#{cron_name}-node2"],
            },
          )
        end
        let(:params) do
          default_params.merge(
            forgets: {
              node1: {
                'runasuser'           => default_runasuser.to_s,
                'repository_url'      => '/var/foo',
                'repository_password' => 'foopass',
              },
            },
          )
        end

        it { is_expected.to contain_restic__forget('node1') }
        it { is_expected.to contain_cron("#{cron_name}-node2").with_ensure('absent') }
      end
    end
  end
end
