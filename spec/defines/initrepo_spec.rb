require 'spec_helper'

repository_urlremote = 'sftp:user@foohost:/var/foo'
repository_urllocal = '/var/foo'
default_runasuser = 'root'

describe 'restic::initrepo' do
  let(:default_params) do
    {
      runasuser: default_runasuser.to_s,
    }
  end
  let(:title) { 'namevar' }

  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:pre_condition) do
        'include restic'
      end
      let(:facts) do
        os_facts.merge(
          restic_backups: {},
          restic_forgets: {},
        )
      end
      let(:params) do
        default_params.merge(
          repository_url: repository_urllocal.to_s,
          repository_password: 'secret',
        )
      end

      it { is_expected.to compile }

      context 'with repository type unmanaged' do
        let(:params) do
          default_params.merge(
            repository_url: repository_urlremote.to_s,
            repository_password: 'secret',
          )
        end

        it { is_expected.to compile.and_raise_error(%r{unsupported repository #{repository_urlremote}, only local repository accepted}) }
      end
    end
  end
end
