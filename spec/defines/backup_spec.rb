require 'spec_helper'

describe 'restic::backup' do
  let(:pre_condition) do
    'include restic'
  end
  let(:title) { 'namevar' }
  let(:params) do
    {
      runasuser: 'root',
      repository_url: '/myrepo',
      repository_password: 'secret',
      gogc: 10,
    }
  end

  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it { is_expected.to compile }
    end
  end
end
