require 'spec_helper_acceptance'

restic_bin = '/usr/local/bin/restic'
restic_initial_version = '0.13.0'
restic_upgraded_version = '0.14.0'
restic_upgraded_checksum = 'c8da7350dc334cd5eaf13b2c9d6e689d51e7377ba1784cc6d65977bd44ee1165'
wrapper_dir = '/opt/restic'
wrapperbackup_script = "#{wrapper_dir}/backupwrapper"
wrapperforget_script = "#{wrapper_dir}/forgetwrapper"
restic_cronlogdir = '/var/log/restic'
logrotate_config = '/etc/logrotate.d/restic'
restic_cachedir = '/tmp/restic-cache'

case fact('os.family')
when 'Debian'
  crontab_path = '/var/spool/cron/crontabs'
when 'RedHat'
  crontab_path = '/var/spool/cron'
end

describe 'restic class' do
  context 'with defaults parameters' do
    pp = 'include restic'

    it 'applies with no error' do
      apply_manifest(pp, catch_failures: true)
    end
    it 'applies idempotently' do
      apply_manifest(pp, catch_changes: true)
    end

    describe command('restic version') do
      its(:exit_status) { is_expected.to eq 0 }
      its(:stdout) { is_expected.to match %r{restic #{restic_initial_version}} }
    end

    describe file(restic_bin.to_s) do
      it { is_expected.to be_file }
      it { is_expected.to be_owned_by 'root' }
      it { is_expected.to be_grouped_into 'root' }
      it { is_expected.to be_mode 750 }
    end

    describe file(restic_cronlogdir.to_s) do
      it { is_expected.to be_directory }
      it { is_expected.to be_owned_by 'root' }
      it { is_expected.to be_grouped_into 'root' }
      it { is_expected.to be_mode 750 }
    end

    describe file(logrotate_config.to_s) do
      it { is_expected.to be_file }
      it { is_expected.to be_owned_by 'root' }
      it { is_expected.to be_grouped_into 'root' }
      it { is_expected.to be_mode 644 }
      it { is_expected.to contain("#{restic_cronlogdir}/*.log") }
    end
  end

  context 'logrotate disabled' do
    pp = <<-MANIFEST
      class { 'restic':
        logrotate => false,
      }
    MANIFEST

    it 'applies with no error' do
      apply_manifest(pp, catch_failures: true)
    end
    it 'applies idempotently' do
      apply_manifest(pp, catch_changes: true)
    end

    describe file(restic_bin.to_s) do
      it { is_expected.to be_file }
      it { is_expected.to be_owned_by 'root' }
      it { is_expected.to be_grouped_into 'root' }
      it { is_expected.to be_mode 750 }
    end

    describe file(restic_cronlogdir.to_s) do
      it { is_expected.to be_directory }
      it { is_expected.to be_owned_by 'root' }
      it { is_expected.to be_grouped_into 'root' }
      it { is_expected.to be_mode 750 }
    end

    describe file(logrotate_config.to_s) do
      it { is_expected.not_to be_file }
    end
  end

  context 'parameter runasuser set to user not existing' do
    it 'fails' do
      pp = <<-MANIFEST
        class { 'restic':
          runasuser => 'foo',
        }
      MANIFEST
      apply_manifest(pp, expect_failures: true)
    end
  end

  context 'parameter runasuser set to an existing user' do
    pp = <<-MANIFEST
      class { 'restic':
        runasuser => 'bar',
      }
    MANIFEST

    it 'applies with no error' do
      apply_manifest(pp, catch_failures: true)
    end
    it 'applies idempotently' do
      apply_manifest(pp, catch_changes: true)
    end

    describe file(restic_bin.to_s) do
      it { is_expected.to be_file }
      it { is_expected.to be_owned_by 'root' }
      it { is_expected.to be_grouped_into 'bar' }
      it { is_expected.to be_mode 750 }
    end

    describe 'rectic binary is_expected.to be with capabilities' do
      context command("getcap #{restic_bin}") do
        its(:stdout) { is_expected.to match %r{^#{restic_bin}.*\s+cap_dac_read_search} }
      end
    end

    describe file(restic_cronlogdir.to_s) do
      it { is_expected.to be_directory }
      it { is_expected.to be_owned_by 'bar' }
      it { is_expected.to be_grouped_into 'bar' }
      it { is_expected.to be_mode 750 }
    end

    describe file(logrotate_config.to_s) do
      it { is_expected.to be_file }
      it { is_expected.to be_owned_by 'root' }
      it { is_expected.to be_grouped_into 'root' }
      it { is_expected.to be_mode 644 }
      it { is_expected.to contain("#{restic_cronlogdir}/*.log") }
    end
  end

  context 'backups without root privileges and with custon cache dir' do
    pp = <<-MANIFEST
      $reporand=fqdn_rand(1000, 'hakuna matata')
      $reponame="repo${reporand}"
      $repourl="sftp:bar@127.0.0.1:/${reponame}"
      $repopwd='barpass'
      class { 'restic' :
        runasuser           => 'bar',
        repository_url      => "/${reponame}",
        repository_password => $repopwd,
        wrapper_path        => '#{wrapper_dir}',
        cache_dir           => '#{restic_cachedir}',
        backups             => {
          'projet1' => {
             cron_frequency => 'hourly',
             excludes       => ['/sys','/proc','/dev'],
           },
          'projet2-aa' => {
            repository_url      => $repourl,
            cron_frequency      => 'daily',
            cron_hour           => 1,
            cron_minute         => 42,
          },
          'projet2-bb' => {
            repository_url      => $repourl,
            cron_frequency      => 'weekly',
            cron_hour           => 4,
            cron_minute         => 42,
            cron_weekday        => '2',
          },
          'projet2-cc' => {
            repository_url      => $repourl,
            cron_frequency      => 'monthly',
            cron_hour           => 4,
            cron_minute         => 42,
            cron_monthday       => '3',
            inodes              => ['/usr'],
          },
        },
      }
    MANIFEST

    it 'applies with no error' do
      apply_manifest(pp, catch_failures: true)
    end
    it 'applies idempotently' do
      apply_manifest(pp, catch_changes: true)
    end

    describe cron do
      it { is_expected.to have_entry("42 1 * * * #{wrapperbackup_script}-projet2-aa >> #{restic_cronlogdir}/projet2-aa.log 2>&1").with_user('bar') }
      it { is_expected.to have_entry("42 4 * * 2 #{wrapperbackup_script}-projet2-bb >> #{restic_cronlogdir}/projet2-bb.log 2>&1").with_user('bar') }
      it { is_expected.to have_entry("42 4 3 * * #{wrapperbackup_script}-projet2-cc >> #{restic_cronlogdir}/projet2-cc.log 2>&1").with_user('bar') }
    end

    describe file('/etc/restic') do
      it { is_expected.to be_directory }
      it { is_expected.to be_mode 700 }
      it { is_expected.to be_owned_by 'bar' }
    end
    describe command('ls /etc/restic/daily-*') do
      its(:exit_status) { is_expected.to eq 0 }
    end
    describe command('ls /etc/restic/weekly-*') do
      its(:exit_status) { is_expected.to eq 0 }
    end
    describe command('ls /etc/restic/monthly-*') do
      its(:exit_status) { is_expected.to eq 0 }
    end

    describe file("#{wrapperbackup_script}-projet1") do
      it { is_expected.to be_file }
      it { is_expected.to be_mode 500 }
      it { is_expected.to be_owned_by 'bar' }
      it { is_expected.to contain(' --tag projet1 --tag hourly --exclude /sys --exclude /proc --exclude /dev /').after(%r{^restic backup }) }
    end
    describe file("#{wrapperbackup_script}-projet2-aa") do
      it { is_expected.to be_file }
      it { is_expected.to be_mode 500 }
      it { is_expected.to be_owned_by 'bar' }
      it { is_expected.to contain(' --tag projet2-aa --tag daily --exclude /proc --exclude /sys --exclude /dev --exclude /mnt --exclude /media /').after(%r{^restic backup }) }
    end
    describe file("#{wrapperbackup_script}-projet2-bb") do
      it { is_expected.to be_file }
      it { is_expected.to be_mode 500 }
      it { is_expected.to be_owned_by 'bar' }
      it { is_expected.to contain(' --tag projet2-bb --tag weekly --exclude /proc --exclude /sys --exclude /dev --exclude /mnt --exclude /media /').after(%r{^restic backup }) }
    end
    describe file("#{wrapperbackup_script}-projet2-cc") do
      it { is_expected.to be_file }
      it { is_expected.to be_mode 500 }
      it { is_expected.to be_owned_by 'bar' }
      it { is_expected.to contain(' --tag projet2-cc --tag monthly --exclude /proc --exclude /sys --exclude /dev --exclude /mnt --exclude /media /usr').after(%r{^restic backup }) }
    end
    describe command("su - bar -c #{wrapperbackup_script}-projet1") do
      its(:exit_status) { is_expected.to eq 0 }
    end
    describe file(restic_cachedir.to_s) do
      it { is_expected.to be_directory }
      it { is_expected.to be_owned_by 'bar' }
      it { is_expected.to be_mode 700 }
    end
    describe file("#{restic_cachedir}/CACHEDIR.TAG") do
      it { is_expected.to be_file }
      it { is_expected.to be_owned_by 'bar' }
      it { is_expected.to contain 'Signature:' }
    end
  end

  context 'backups without root privileges' do
    pp = <<-MANIFEST
      # this repository is used later as a repo2 during init of a repository copy.
      $repourl="/repository_one"
      $repouser='bar'
      $repopwd='barpass1'
      class { 'restic' :
        runasuser           => "$repouser",
        repository_url      => "$repourl",
        repository_password => "$repopwd",
        wrapper_path        => '#{wrapper_dir}',
        backups => {
          'projet1' => {
             cron_frequency => 'hourly',
             excludes       => ['/sys','/proc','/dev'],
          },
          'projet2-aa' => {
            repository_url      => "sftp:${repouser}@127.0.0.1:${repourl}",
            repository_password => "$repopwd",
            cron_frequency      => 'daily',
            cron_hour           => 1,
            cron_minute         => 42,
          },
        },
      }
    MANIFEST

    it 'removes undiscribed backups with no error' do
      apply_manifest(pp, catch_failures: true)
    end
    it 'removes undiscribed backups idempotently' do
      apply_manifest(pp, catch_changes: true)
    end

    describe cron do
      it { is_expected.to have_entry("42 1 * * * #{wrapperbackup_script}-projet2-aa >> #{restic_cronlogdir}/projet2-aa.log 2>&1").with_user('bar') }
      it { is_expected.not_to have_entry("42 4 * * 2 #{wrapperbackup_script}-projet2-bb >> #{restic_cronlogdir}/projet2-bb.log 2>&1").with_user('bar') }
      it { is_expected.not_to have_entry("42 4 * * 3 #{wrapperbackup_script}-projet2-cc >> #{restic_cronlogdir}/projet2-cc.log 2>&1").with_user('bar') }
    end
    describe command('ls /etc/restic/weekly-*') do
      its(:exit_status) { is_expected.not_to eq 0 }
    end
    describe command('ls /etc/restic/monthly-*') do
      its(:exit_status) { is_expected.not_to eq 0 }
    end

    describe file("#{wrapperbackup_script}-projet1") do
      it { is_expected.to be_file }
      it { is_expected.to be_mode 500 }
      it { is_expected.to be_owned_by 'bar' }
      it { is_expected.to contain(' --tag projet1 --tag hourly --exclude /sys --exclude /proc --exclude /dev /').after(%r{^restic backup }) }
    end
    describe file("#{wrapperbackup_script}-projet2-aa") do
      it { is_expected.to be_file }
      it { is_expected.to be_mode 500 }
      it { is_expected.to be_owned_by 'bar' }
      it { is_expected.to contain(' --tag projet2-aa --tag daily --exclude /proc --exclude /sys --exclude /dev --exclude /mnt --exclude /media /').after(%r{^restic backup }) }
    end
    describe file("#{crontab_path}/bar") do
      it { is_expected.to contain('GOGC=10') }
    end

    describe file("#{wrapperbackup_script}-projet2-bb") do
      it { is_expected.not_to be_file }
    end
    describe file("#{wrapperbackup_script}-projet2-cc") do
      it { is_expected.not_to be_file }
    end
  end

  context 'forgets without root privileges' do
    pp = <<-MANIFEST
      $reporand=fqdn_rand(1000, 'hakuna maaataaaaataa')
      $reponame="repo${reporand}"
      class { 'restic' :
        runasuser    => 'bar',
        wrapper_path => '#{wrapper_dir}',
        forgets => {
          'thehost1' => {
            repository_url      => "/${reponame}",
            repository_password => 'barpass',
            prune               => false,
          },
          'thehost2-bb' => {
            repository_url      => "sftp:bar@127.0.0.1:/${reponame}",
            repository_password => 'barpass',
            cron_frequency      => 'weekly',
            cron_minute         => 42,
            cron_hour           => 4,
            cron_weekday        => '2',
            keeps               => {
              'daily'  => 14,
              'weekly' => 3,
            },
          },
          'thehost2-cc' => {
            repository_url      => "sftp:bar@127.0.0.1:/${reponame}",
            repository_password => 'barpass',
            cron_frequency      => 'monthly',
            cron_minute         => 42,
            cron_hour           => 4,
            cron_monthday       => '3',
            keeps               => {
              'weekly'  => 3,
              'monthly' => 5,
              'yearly'  => 12,
              'tag'     => 'lts',
            },
          },
        },
      }
    MANIFEST

    it 'applies with no error' do
      apply_manifest(pp, catch_failures: true)
    end
    it 'applies idempotently' do
      apply_manifest(pp, catch_changes: true)
    end

    describe cron do
      it { is_expected.to have_entry("42 4 * * 2 #{wrapperforget_script}-thehost2-bb >> #{restic_cronlogdir}/forget-thehost2-bb.log 2>&1").with_user('bar') }
      it { is_expected.to have_entry("42 4 3 * * #{wrapperforget_script}-thehost2-cc >> #{restic_cronlogdir}/forget-thehost2-cc.log 2>&1").with_user('bar') }
    end
    describe file("#{wrapperforget_script}-thehost1") do
      it { is_expected.to be_file }
      it { is_expected.to be_mode 500 }
      it { is_expected.to be_owned_by 'bar' }
      it { is_expected.to contain('--keep-daily 7 --keep-weekly 4').after(%r{^restic forget }) }
      it { is_expected.not_to contain('--prune').after(%r{^restic forget }) }
    end
    describe file("#{wrapperforget_script}-thehost2-bb") do
      it { is_expected.to be_file }
      it { is_expected.to be_mode 500 }
      it { is_expected.to be_owned_by 'bar' }
      it { is_expected.to contain('--keep-daily 14 --keep-weekly 3 --prune').after(%r{^restic forget }) }
    end
    describe file("#{wrapperforget_script}-thehost2-cc") do
      it { is_expected.to be_file }
      it { is_expected.to be_mode 500 }
      it { is_expected.to be_owned_by 'bar' }
      it { is_expected.to contain('--keep-weekly 3 --keep-monthly 5 --keep-yearly 12 --keep-tag lts --prune').after(%r{^restic forget }) }
    end
  end

  context 'forgets without root privileges' do
    pp = <<-MANIFEST
      $reporand=fqdn_rand(1000, 'hakuna maaataaaaataa')
      $reponame="repo${reporand}"
      class { 'restic' :
        runasuser    => 'bar',
        wrapper_path => '#{wrapper_dir}',
        forgets => {
          'thehost1' => {
            repository_url      => "/${reponame}",
            repository_password => 'barpass',
            prune               => false,
          },
          'thehost2-bb' => {
            repository_url      => "sftp:bar@127.0.0.1:/${reponame}",
            repository_password => 'barpass',
            cron_frequency      => 'weekly',
            cron_frequency      => 'weekly',
            cron_minute         => 42,
            cron_hour           => 4,
            cron_weekday        => '2',
            keeps               => {
              'daily'  => 14,
              'weekly' => 3,
            },
            prune               => false,
          },
        },
      }
    MANIFEST

    it 'removes undescribed forgets with no error' do
      apply_manifest(pp, catch_failures: true)
    end
    it ' removes undescribed forgets  idempotently' do
      apply_manifest(pp, catch_changes: true)
    end

    describe cron do
      it { is_expected.to have_entry("42 4 * * 2 #{wrapperforget_script}-thehost2-bb >> #{restic_cronlogdir}/forget-thehost2-bb.log 2>&1").with_user('bar') }
      it { is_expected.not_to have_entry("42 4 3 * * #{wrapperforget_script}-thehost2-cc >> #{restic_cronlogdir}/forget-thehost2-cc.log 2>&1").with_user('bar') }
    end
    describe file("#{wrapperforget_script}-thehost1") do
      it { is_expected.to be_file }
      it { is_expected.to be_mode 500 }
      it { is_expected.to be_owned_by 'bar' }
      it { is_expected.to contain('--keep-daily 7 --keep-weekly 4').after(%r{^restic forget }) }
      it { is_expected.not_to contain('--prune').after(%r{^restic forget }) }
    end
    describe file("#{wrapperforget_script}-thehost2-bb") do
      it { is_expected.to be_file }
      it { is_expected.to be_mode 500 }
      it { is_expected.to be_owned_by 'bar' }
      it { is_expected.to contain('--keep-daily 14 --keep-weekly 3').after(%r{^restic forget }) }
      it { is_expected.not_to contain('--prune').after(%r{^restic forget }) }
    end
    describe file("#{wrapperforget_script}-thehost2-cc") do
      it { is_expected.not_to be_file }
    end
  end

  context 'with restic version upgraded' do
    pp = <<-MANIFEST
      class { 'restic':
        version  => '#{restic_upgraded_version}',
        checksum => '#{restic_upgraded_checksum}',
      }
    MANIFEST
    it 'applies with no error' do
      apply_manifest(pp, catch_failures: true)
    end
    it 'applies idempotently' do
      apply_manifest(pp, catch_changes: true)
    end

    describe command('restic version') do
      its(:exit_status) { is_expected.to eq 0 }
      its(:stdout) { is_expected.to match %r{restic #{restic_upgraded_version}} }
    end
  end

  context 'with several repositories' do
    pp = <<-MANIFEST
      class { 'restic':
        repositories => [
          {repository_url => '/repo_a', repository_password => 'pwd_a'},
          {repository_url => '/repo_b', repository_password => 'pwd_b'},
          {repository_url => '/repo_c', repository_password => 'pwd_c'},
        ],
      }
    MANIFEST
    it 'applies with no error' do
      apply_manifest(pp, catch_failures: true)
    end
    it 'applies idempotently' do
      apply_manifest(pp, catch_changes: true)
    end

    describe command('RESTIC_PASSWORD="pwd_a" restic snapshots --no-lock --repo /repo_a') do
      its(:exit_status) { is_expected.to eq 0 }
    end
  end

  context 'without root privileges and with a copy repository and forgets' do
    pp = <<-MANIFEST
      class { 'restic' :
        runasuser            => 'bar',
        repository_url       => '/repository_one',
        repository_password  => 'barpass1',
        repository2_url      => '/repository_copy',
        repository2_password => 'barpass2',
        wrapper_path         => '#{wrapper_dir}',
        forgets              => {
          global => {
            cron_frequency => 'weekly',
            cron_minute    => 42,
            cron_hour      => 4,
            cron_weekday   => '2',
            keeps          => {
              daily   => 7,
              weekly  => 4,
              monthly => 3,
            },
          },
        },
      }
    MANIFEST
    it 'applies with no error' do
      apply_manifest(pp, catch_failures: true)
    end
    it 'applies idempotently' do
      apply_manifest(pp, catch_changes: true)
    end

    describe cron do
      it { is_expected.to have_entry("42 4 * * 2 #{wrapperforget_script}-global >> #{restic_cronlogdir}/forget-global.log 2>&1").with_user('bar') }
    end

    describe command('RESTIC_PASSWORD="barpass1" restic snapshots --no-lock --repo /repository_one') do
      its(:exit_status) { is_expected.to eq 0 }
    end
    describe command('RESTIC_PASSWORD="barpass2" restic snapshots --no-lock --repo /repository_copy') do
      its(:exit_status) { is_expected.to eq 0 }
    end
    describe command("#{wrapperforget_script}-global") do
      its(:exit_status) { is_expected.to eq 0 }
    end
  end
end
