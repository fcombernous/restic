#
# @summary Creates Restic repositories
#
# @param repositories A list of repositories.
#
# @param runasuser The user owner of data of the repository
#
class restic::initrepos (
  Array[Restic::Repository] $repositories = $restic::repositories,
  String $runasuser = $restic::runasuser,
) {
  $repositories.each | Restic::Repository $_repo | {
    $_repo_url = $_repo['repository_url']
    $_repo_pwd = $_repo['repository_password']

    restic::initrepo { $_repo_url:
      repository_url      => $_repo_url,
      repository_password => $_repo_pwd,
      runasuser           => $runasuser,
    }
  }
}
