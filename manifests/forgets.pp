#
# @summary Creates cron tasks to unreference or delete snapshots
#
# @param forgets
#   A hash of forgets.
#   See defined type `restic::forget` documentation to know required informations to define a forget task.
#
# @param runasuser A user account that own the cron tasks.
#
class restic::forgets (
  Hash $forgets = {},
  String $runasuser = $restic::runasuser,
) {
  # if a backup or forget is not defined remove the corresponding cron and wrapper
  $_all_forgets_in_facts = assert_type(Array, $facts['restic_forgets'][$runasuser]) |$expected, $actual| { [] }
  $_defined_forgets_in_facts = delete($_all_forgets_in_facts, 'restic-forget-global')
  $_keys_in_forgets = $forgets.map | $_items | { $_items[0] }
  $_defined_forgets_in_forgets = $_keys_in_forgets.map | String $_item | { "restic-forget-${_item}" }

  $_defined_forgets_in_facts.each | String $_forget_in_facts | {
    if !member($_defined_forgets_in_forgets , $_forget_in_facts) {
      cron { $_forget_in_facts :
        ensure => absent,
        user   => $runasuser,
      }
    }
  }

  create_resources('restic::forget', $forgets)
}
