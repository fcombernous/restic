#
# @summary Creates cron tasks to create snapshots
#
# @param backups
#   A hash of backups.
#   See defined type `restic::backup` documentation to know required informations to define a backup task.
#
# @param runasuser A user account that own the cron tasks.
#
# @param gogc
#   Set value for GO garbage collector during backups
#   Restic can be agressive. Default value is safe.
#   With lower value, the OOM killer can kill the backup process under low ressources conditions.
#
class restic::backups (
  Hash $backups = {},
  String $runasuser = $restic::runasuser,
  Integer $gogc = $restic::gogc,
) {
  # if a backup or forget is not defined remove the corresponding cron and wrapper
  $_defined_backups_in_facts = assert_type(Array, $facts['restic_backups'][$runasuser]) |$expected, $actual| { [] }
  $_keys_in_backups = $backups.map | $_items | { $_items[0] }
  $_defined_backups_in_backups = $_keys_in_backups.map | String $_item | { "restic-backup-${_item}" }

  $_defined_backups_in_facts.each | String $_backup_in_facts | {
    if !member($_defined_backups_in_backups , $_backup_in_facts) {
      cron { $_backup_in_facts :
        ensure => absent,
        user   => $runasuser,
      }
    }
  }

  create_resources('restic::backup', $backups)
}
