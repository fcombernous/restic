#
# @summary Creates a restic repository
#
# @param repository_url URL where repository is created (only local repository supported)
#
# @param repository_password Password used to access to repository
#
# @param runasuser The user owner of data of the repository
#
# @param repository2_url
#   URL of second repository considered as reference for chunker settings.
#   The settings of the repository2 are reused for the init of repository.
#
# @param repository2_password Password used to access to repository2.
#
define restic::initrepo (
  Restic::Repositoryurl $repository_url = $restic::repository_url,
  String[1] $repository_password = $restic::repository_password,
  String[1] $runasuser = $restic::runasuser,
  Optional[Restic::Repositoryurl] $repository2_url = undef,
  Optional[String[1]] $repository2_password = undef ,
) {
  $_repository_type = $repository_url ? {
    Pattern['^/'] => 'local',
    default       => undef,
  }

  case $_repository_type {
    'local': {
      file { $repository_url :
        ensure => directory,
        owner  => $runasuser,
        group  => 0,
        mode   => '0750',
      }

      if $repository2_url {
        restic_init { $repository_url :
          ensure    => present,
          password  => $repository_password,
          user      => $runasuser,
          repo2     => $repository2_url,
          password2 => $repository2_password,
          require   => [
            Class['restic::install'],
            File[$repository_url],
          ],
        }
      } else {
        restic_init { $repository_url :
          ensure   => present,
          password => $repository_password,
          user     => $runasuser,
          require  => [
            Class['restic::install'],
            File[$repository_url],
          ],
        }
      }
    }
    default: {
      fail("unsupported repository ${repository_url}, only local repository accepted")
    }
  }
}
