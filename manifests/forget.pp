#
# @summary Creates a Cron task that will call restic `forget` sub command to delete and unreference.
#
# @note With title of resource 'global' forget parse all the repository, if not only hostname used as title.
#
# @param repository_url URL of repository where data is unreferenced and/or deleted.
#
# @param repository_password Password used to access to repository
#
# @param cache_dir Set the cache directory. (default: use system default cache directory)
#
# @param keeps Keeps snapshots as defined in the parameter (See datatype Restic::Forgetkeeps for details)
#
# @param tags Only consider snapshots which include this taglist
#
# @param prune With prune to false, data is only unreferenced and stay in the data store.
#
# @param runasuser The user account used to execute forget command.
#
# @param log_path Directory where cron task send log files produced by Restic backups and forgets.
#
# @param cron_frequency
#   Defines the frequency of cron job for forget task with a simple work (to avoid wrong usage of cron Puppet type).
#   The filled values from cron are provided by $cron_hour, $cron_minute, $cron_weekday, $cron_monthday.
#
# @param cron_minute The minute at which to run the cron forget job.
#
# @param cron_hour The hour at which to run the cron forget job.
#
# @param cron_weekday The weekday on which to run the the cron forget job.
#
# @param cron_monthday The day of the month on which to run the cron forget job.
#
# @param limit_download Limits downloads to a maximum rate in KiB/s during copy to remote repository. (default: unlimited)
#
# @param limit_upload Limits uploads to a maximum rate in KiB/s during copy to remote repository. (default: unlimited)
#
# @param repository2_url
#   URL of second repository considered as reference for chunker settings.
#   The settings of the repository2 are reused for the init of repository.
#
# @param repository2_password Password used to access to repository2.
#
define restic::forget (
  Restic::Repositoryurl $repository_url = $restic::repository_url,
  String[1] $repository_password = $restic::repository_password,
  Restic::Forgetkeeps $keeps = { 'daily' => 7, 'weekly' => 4, 'monthly' => 3 },
  Array[String] $tags = [],
  Boolean $prune = true,
  String $runasuser = $restic::runasuser,
  Optional[Stdlib::Unixpath] $cache_dir = $restic::cache_dir,
  Stdlib::Unixpath $log_path = $restic::log_path,
  Enum['daily','weekly','monthly'] $cron_frequency = 'weekly',
  Integer $cron_minute = fqdn_rand(59),
  Integer $cron_hour = 9 + fqdn_rand(3),
  String $cron_weekday = '*',
  String $cron_monthday = '1',
  Optional[Restic::Repositoryurl] $repository2_url = $restic::repository2_url,
  Optional[String[1]] $repository2_password = $restic::repository2_password,
  Optional[Integer] $limit_download = $restic::limit_download,
  Optional[Integer] $limit_upload = $restic::limit_upload,
) {
  $_hostname = $title
  $_hostnameoption = $_hostname ? {
    Pattern['^global'] => '',
    String             => "--hostname ${title}",
    default            => undef,
  }

  $_forgetscript = assert_type(Stdlib::Unixpath, "${restic::wrapper_path}/forgetwrapper-${_hostname}")

  $_tagsarray = $tags.map |String $_item| { "--tag ${_item}" }
  $_tagoptions = $_tagsarray.reduce |String $_memo, String $_value| { "${_memo} ${_value}" }

  # hash $keeps have the structure defined by custom datatype Restic::Forgetkeeps
  $_keepsarray = $keeps.map |$_key, $_value| { "--keep-${_key} ${_value}" }
  $_keepoptions = $_keepsarray.reduce |String $_memo, String $_value| { "${_memo} ${_value}" }
  $_pruneoption = $prune ? {
    true    => '--prune',
    false   => '',
  }

  $_restic_cron_command = "${_forgetscript} >> ${log_path}/forget-${_hostname}.log 2>&1"

  $_etc_directory = '/etc/restic'
  $_etc_filename = fqdn_rand(30, $repository_url)
  $_etc_restic_file = "${_etc_directory}/${cron_frequency}-${_etc_filename}"

  ensure_resource('file', $_etc_directory,
    {
      'ensure'  => 'directory',
      'mode'    => '0700',
      'owner'   => $runasuser,
      'group'   => 0,
      'purge'   => true,
      'recurse' => true,
      'force'   => true,
    }
  )

  ensure_resource('file', $_etc_restic_file,
    {
      'ensure'  => 'file',
      'mode'    => '0600',
      'owner'   => $runasuser,
      'group'   => 0,
      'content' => epp('restic/restic-etc.epp',
        {
          repository_password  => $repository_password,
          repository2_password => $repository2_password,
          cache_dir            => $cache_dir,
        }
      ),
    }
  )

  if $cache_dir {
    ensure_resource('file', $cache_dir,
      {
        'ensure'  => 'directory',
        'mode'    => '0700',
        'owner'   => $runasuser,
        'group'   => 0,
      }
    )
  }

  file { $_forgetscript:
    ensure  => file,
    content => epp('restic/restic-forget.sh.epp',
      {
        repository_url  => $repository_url,
        hostnameoption  => $_hostnameoption,
        tagoptions      => $_tagoptions,
        keepoptions     => $_keepoptions,
        pruneoption     => $_pruneoption,
        etc_restic_file => $_etc_restic_file,
        repository2_url => $repository2_url,
        limit_upload    => $limit_upload,
        limit_download  => $limit_download,
      }
    ),
    owner   => $runasuser,
    group   => 0,
    mode    => '0500',
    require => Class['restic::install'],
  }

  case $cron_frequency {
    'daily': {
      cron { "restic-forget-${_hostname}":
        ensure  => present,
        command => $_restic_cron_command,
        user    => $runasuser,
        minute  => $cron_minute,
        hour    => $cron_hour,
        weekday => absent,
        require => File[$_forgetscript],
      }
    }
    'weekly': {
      cron { "restic-forget-${_hostname}":
        ensure  => present,
        command => $_restic_cron_command,
        user    => $runasuser,
        minute  => $cron_minute,
        hour    => $cron_hour,
        weekday => $cron_weekday,
        require => File[$_forgetscript],
      }
    }
    'monthly': {
      cron { "restic-forget-${_hostname}":
        ensure   => present,
        command  => $_restic_cron_command,
        user     => $runasuser,
        minute   => $cron_minute,
        hour     => $cron_hour,
        weekday  => absent,
        monthday => $cron_monthday,
        require  => File[$_forgetscript],
      }
    }
    default: {
      fail("unexpected frequency got ${cron_frequency} expects daily, weekly, monthly")
    }
  }
}
