# @summary Install and configure Restic to make backup
#
# @param runasuser Already existing user account that make backups and own the data of repository.
#
# @param repositories Array of repositories.
#
# @param repository_url URL of Restic repository. If you fill repositories parameter this one is ignored.
#
# @param repository_password Password used to access to repository. If you fill repositories parameter this one is ignored.
#
# @param limit_download Limits downloads to a maximum rate in KiB/s during copy to remote repository. (default: unlimited)
#
# @param limit_upload Limits uploads to a maximum rate in KiB/s during copy to remote repository. (default: unlimited)
#
# @param repository2_url
#   URL of second repository considered as reference for chunker settings.
#   The settings of the repository2 are reused for the init of repository.
#
# @param repository2_password Password used to access to repository2.
#
# @param cache_dir Set the cache directory. (default: use system default cache directory)
#
# @param backups Creates cron tasks to make backups snapshots.
#
# @param forgets Creates cron tasks to unreference or delete snapshots.
#
# @param logrotate Enable logrotate from restic log files.
#
# @param log_path Directory where cron task send log files produced by Restic backups and forgets.
#
# @param gogc
#   Set value for GO garbage collector during backups
#   Restic can be agressive. Default value is safe.
#   With lower value, the OOM killer can kill the backup process under low ressources conditions.
#
# @param binary_path
#   The path where is installed the Restic binary. The binary MUST be in path of the user running `restic` command.
#
# @param wrapper_path
#   Restic commands are launched through wrappers.
#   The define the path were wrappers are installed.
#   CAUTION : In this path, all inodes not under puppet control are deleted.
#
# @param version The installed version of Restic. (Modify version number after installation is ignored).
#
# @param checksum The checksum of the download restic archive.
#
# @param manage_bzip2 the module install bzip2 when set to true.
#
# @param ssh_allowed_returned_values list of allowed returned value with ssh command check connection.
#
class restic (
  Optional[Restic::Repositoryurl] $repository_url = undef,
  Optional[String[1]] $repository_password = undef,
  Optional[Restic::Repositoryurl] $repository2_url = undef,
  Optional[String[1]] $repository2_password = undef,
  Optional[Integer] $limit_download = undef,
  Optional[Integer] $limit_upload = undef,
  Optional[Stdlib::Unixpath] $cache_dir = undef,
  String $runasuser = 'root',
  Array[Restic::Repository] $repositories = [],
  Hash $backups = {},
  Hash $forgets = {},
  Boolean $logrotate = true,
  Stdlib::Unixpath $log_path = '/var/log/restic',
  Integer $gogc = 10,
  Stdlib::Unixpath $binary_path = '/usr/local/bin',
  Stdlib::Unixpath $wrapper_path = "${facts['puppet_vardir']}/restic",
  Pattern['^\d+.\d+.\d+$'] $version = '0.13.0',
  String $checksum = '514d0711317427f45d3ca23e66cf66e9f98caef660314d843f59b38511e94a2c',
  Boolean $manage_bzip2 = true,
  Array $ssh_allowed_returned_values = ['0'],
) {
  class { 'restic::install':
    runasuser    => $runasuser,
    logrotate    => $logrotate,
    binary_path  => $binary_path,
    wrapper_path => $wrapper_path,
    version      => $version,
    log_path     => $log_path,
    manage_bzip2 => $manage_bzip2,
    checksum     => $checksum,
  }

  if length($repositories) != 0 {
    class { 'restic::initrepos':
      repositories => $repositories,
      require      => Class['restic::install'],
    }
  } else {
    if $repository_url and $repository_password {
      restic::initrepo { $repository_url:
        repository_url      => $repository_url,
        repository_password => $repository_password,
        require             => Class['restic::install'],
      }
    }
    if $repository2_url and $repository2_password {
      if $repository_url and $repository_password {
        $_repository_type = $repository2_url ? {
          Pattern['^/'] => 'local',
          default       => undef,
        }

        case $_repository_type {
          'local': {
            # repository_url is a copy repository configurer with same chunker settings used in repository2_url
            restic::initrepo { $repository2_url:
              repository_url       => $repository2_url,
              repository_password  => $repository2_password,
              repository2_url      => $repository_url,
              repository2_password => $repository_password,
              require              => Class['restic::install'],
            }
          }
          default: {
            # restic::initrepo support only local repository we have nothing to do.
          }
        }
      } else {
        fail('Missing parameters repository_url and repository_password to prepare copy with chunker settings used in repository2')
      }
    }
  }

  if ! empty($backups) {
    class { 'restic::backups':
      backups => $backups,
      require => Class['restic::install'],
    }
  }

  if ! empty($forgets) {
    class { 'restic::forgets':
      forgets => $forgets,
      require => Class['restic::install'],
    }
  }
}
