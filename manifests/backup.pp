#
# @summary Creates a Cron task that will call `restic backup` sub command to snapshot.
#
# @param repository_url URL of repository where data is stored.
#
# @param repository_password Password used to access to repository
#
# @param cache_dir Set the cache directory. (default: use system default cache directory)
#
# @param tags tags snapshot with this taglist
#
# @param runasuser The user account used to execute forget command.
#
# @param inodes list of inodes thet must be included into snapshot.
#
# @param excludes List of inodes that must be excluded from snapshot.
#
# @param gogc
#   Set value for GO garbage collector during backups
#   Restic can be agressive. Default value is safe.
#   With lower value, the OOM killer can kill the backup process under low ressources conditions.
#
# @param log_path Directory where cron task send log files produced by Restic backups and forgets.
#
# @param cron_frequency
#   Defines the frequency of cron job for backup task with a simple work (to avoid wrong usage of cron Puppet type).
#   The filled values from cron are provided by $cron_hour, $cron_minute, $cron_weekday, $cron_monthday.
#
# @param cron_minute The minute at which to run the cron backup job.
#
# @param cron_hour The hour at which to run the cron backup job.
#
# @param cron_weekday The weekday on which to run the the cron backup job.
#
# @param cron_monthday The day of the month on which to run the cron backup job.
#
define restic::backup (
  Restic::Repositoryurl $repository_url = $restic::repository_url,
  String[1] $repository_password = $restic::repository_password,
  Array[String[1]] $tags = [],
  String[1] $runasuser = $restic::runasuser,
  Optional[Stdlib::Unixpath] $cache_dir = $restic::cache_dir,
  Array[Stdlib::Unixpath] $inodes = ['/'],
  Array[Stdlib::Unixpath] $excludes = ['/proc','/sys','/dev','/mnt','/media'],
  Integer $gogc = $restic::gogc,
  Stdlib::Unixpath $log_path = $restic::log_path,
  Enum['hourly','daily','weekly','monthly'] $cron_frequency = 'daily',
  Integer $cron_minute = fqdn_rand(59),
  Integer $cron_hour = 20 + fqdn_rand(3),
  String $cron_weekday = '6',
  String $cron_monthday = '1',
) {
  # put all provided tags (in the title and in the tag attribute) in only one array
  $_primarytag = $title

  $_repository_type = $repository_url ? {
    Stdlib::Unixpath  => 'local',
    Pattern['^sftp:'] => 'sftp',
    default           => undef,
  }

  $_alltags = $_primarytag ? {
    Array[String] => union($tags, $_primarytag, [$cron_frequency]),  # as builted-in ressources it is possible to use array as title
    String        => concat($tags, $_primarytag, $cron_frequency), # a string is provided as title of this defined ressource
    default       => fail('unexpected type in title of defined ressource restic::backup'),
  }

  $_backupscript = assert_type(Stdlib::Unixpath, "${restic::wrapper_path}/backupwrapper")
  $_tagoptions = map($_alltags) |$_tag| { "--tag ${_tag}" }
  $_excludeoptions = map($excludes) |$_exclude| { "--exclude ${_exclude}" }

  $_etc_directory = '/etc/restic'
  $_etc_filename = fqdn_rand(30, $repository_url)
  $_etc_restic_file = "${_etc_directory}/${cron_frequency}-${_etc_filename}"

  ensure_resource('file', $_etc_directory,
    {
      'ensure'  => 'directory',
      'mode'    => '0700',
      'owner'   => $runasuser,
      'group'   => 0,
      'purge'   => true,
      'recurse' => true,
      'force'   => true,
    }
  )

  ensure_resource('file', $_etc_restic_file,
    {
      'ensure'  => 'file',
      'mode'    => '0600',
      'owner'   => $runasuser,
      'group'   => 0,
      'content' => epp('restic/restic-etc.epp',
        {
          repository_password => $repository_password,
          cache_dir           => $cache_dir,
        }
      ),
    }
  )

  if $cache_dir {
    ensure_resource('file', $cache_dir,
      {
        'ensure'  => 'directory',
        'mode'    => '0700',
        'owner'   => $runasuser,
        'group'   => 0,
      }
    )
  }

  file { "${_backupscript}-${_primarytag}":
    ensure  => file,
    content => epp('restic/restic-backup.sh.epp',
      {
        repository_url  => $repository_url,
        inodes          => $inodes,
        tagoptions      => $_tagoptions,
        excludeoptions  => $_excludeoptions,
        etc_restic_file => $_etc_restic_file,
      }
    ),
    owner   => $runasuser,
    group   => 0,
    mode    => '0500',
    require => Class['restic::install'],
  }

  case $_repository_type {
    'local': {
      exec { "check-local-repository-by-user-${runasuser}-for-${_primarytag}":
        environment => ["RESTIC_PASSWORD=${repository_password}"],
        path        => '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin',
        command     => "restic --repo ${repository_url} snapshots",
        user        => $runasuser,
        subscribe   => File["${_backupscript}-${_primarytag}"],
        refreshonly => true,
      }
    }
    'sftp': {
      $_address_resticrepository = regsubst($repository_url, '^sftp:[\d\w]+@([\d\w\.]+):/[^/]+.*','\1')
      $_dataowner_resticrepository = regsubst($repository_url, '^sftp:([\d\w]+)@[\d\w\.]+:/[^/]+.*','\1')

      exec { "check-ssh-connection-to-${_dataowner_resticrepository}@${_address_resticrepository}-by-user-${runasuser}-for-${_primarytag}":
        command   => "ssh -oStrictHostKeyChecking=no ${_dataowner_resticrepository}@${_address_resticrepository}",
        path      => '/usr/sbin:/usr/bin:/sbin:/bin',
        user      => $runasuser,
        unless    => "ssh-keygen -l -F ${_address_resticrepository}",
        subscribe => File["${_backupscript}-${_primarytag}"],
        returns   => $restic::ssh_allowed_returned_values,
      }
    }
    default: {
      fail("unknown repository type : ${_repository_type}")
    }
  }

  $_restic_cron_command = "${_backupscript}-${_primarytag} >> ${log_path}/${_primarytag}.log 2>&1"

  case $cron_frequency {
    'hourly': {
      cron { "restic-backup-${_primarytag}":
        ensure      => present,
        environment => "GOGC=${gogc}",
        command     => $_restic_cron_command,
        user        => $runasuser,
        minute      => $cron_minute,
        hour        => absent,
        weekday     => absent,
        require     => File["${_backupscript}-${_primarytag}"],
      }
    }
    'daily': {
      cron { "restic-backup-${_primarytag}":
        ensure      => present,
        environment => "GOGC=${gogc}",
        command     => $_restic_cron_command,
        user        => $runasuser,
        minute      => $cron_minute,
        hour        => $cron_hour,
        weekday     => absent,
        require     => File["${_backupscript}-${_primarytag}"],
      }
    }
    'weekly': {
      cron { "restic-backup-${_primarytag}":
        ensure      => present,
        environment => "GOGC=${gogc}",
        command     => $_restic_cron_command,
        user        => $runasuser,
        minute      => $cron_minute,
        hour        => $cron_hour,
        weekday     => $cron_weekday,
        require     => File["${_backupscript}-${_primarytag}"],
      }
    }
    'monthly': {
      cron { "restic-backup-${_primarytag}":
        ensure      => present,
        environment => "GOGC=${gogc}",
        command     => $_restic_cron_command,
        user        => $runasuser,
        minute      => $cron_minute,
        hour        => $cron_hour,
        weekday     => absent,
        monthday    => $cron_monthday,
        require     => File["${_backupscript}-${_primarytag}"],
      }
    }
    default: {
      fail("unexpected frequency got ${cron_frequency}")
    }
  }
}
