#
# @summary Installs restic binary. Configures capabilities and logrotate
#
# @param runasuser User used to make the backup.
#
# @param logrotate Enable logrotate from restic log files.
#
# @param binary_path The path where is installed the Restic binary. The binary MUST be in path of the user running `restic` command.
#
# @param wrapper_path
#   Restic commands are launched through wrappers.
#   The define the path were wrappers are installed.
#   CAUTION : In this path, all inodes not under puppet control are deleted.
#
# @param manage_bzip2 the module install bzip2 when set to true.
#
# @api private
#
class restic::install (
  String $runasuser = $restic::runasuser,
  Boolean $logrotate = $restic::logrotate,
  Stdlib::Unixpath $log_path = $restic::log_path,
  Stdlib::Unixpath $binary_path = $restic::binary_path,
  Stdlib::Unixpath $wrapper_path = $restic::wrapper_path,
  Pattern['^\d+.\d+.\d+$'] $version = $restic::version,
  String $checksum = $restic::checksum,
  Boolean $manage_bzip2 = $restic::manage_bzip2,
) {
  assert_private()

  $_download_directory = $binary_path
  $_downloaded_filename = "restic_${version}_linux_amd64"
  $_downloaded_filetype = 'bz2'
  $_binary_name = 'restic'
  $_download_url = "https://github.com/restic/restic/releases/download/v${version}/${_downloaded_filename}.${_downloaded_filetype}"
  $_checksum_type = 'sha256'

  case $facts['os']['family'] {
    'Debian': {
      $capabilities_packages = 'libcap2-bin'
      $bunzip2_package = 'bzip2'
    }
    'RedHat': {
      $capabilities_packages = 'libcap'
      $bunzip2_package = 'bzip2'
    }
    default: {
      fail("unsupported OS ${facts['os']['name']}")
    }
  }

  file { $wrapper_path:
    ensure  => directory,
    owner   => $runasuser,
    group   => $runasuser,
    mode    => '0750',
    recurse => true,
    purge   => true,
  }

  # install restic binary
  package { 'restic':
    ensure => absent,
  }

  if $manage_bzip2 {
    package { $bunzip2_package:
      ensure => present,
      notify => Archive[$_downloaded_filename],
    }
  }

  archive { $_downloaded_filename :
    path          => "${_download_directory}/${_downloaded_filename}.${_downloaded_filetype}",
    source        => $_download_url,
    checksum      => $checksum,
    checksum_type => $_checksum_type,
    extract       => true,
    extract_path  => $binary_path,
    creates       => "${binary_path}/${_downloaded_filename}",
  }
  # file type does not handle hard links. So we have to handle it manualy
  exec { "rm -f ${binary_path}/${_binary_name}":
    path        => '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin',
    refreshonly => true,
    subscribe   => Archive[$_downloaded_filename],
  }
  -> exec { "ln ${binary_path}/${_downloaded_filename} ${binary_path}/${_binary_name}":
    path        => '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin',
    refreshonly => true,
    subscribe   => Archive[$_downloaded_filename],
  }

  # setup capabalities if needed
  if $runasuser != 'root' {
    package { $capabilities_packages :
      ensure => present,
    }
    -> file { "${binary_path}/${_binary_name}" :
      ensure  => file,
      owner   => 0,
      group   => $runasuser,
      mode    => '0750',
      require => [
        Archive[$_downloaded_filename],
        Exec["ln ${binary_path}/${_downloaded_filename} ${binary_path}/${_binary_name}"],
      ],
    }
    -> exec { 'set-required-capability':
      path    => '/usr/sbin:/usr/bin:/sbin:/bin',
      command => "setcap cap_dac_read_search=+ep ${binary_path}/${_binary_name}",
      unless  => "getcap ${binary_path}/${_binary_name} | grep -qP '^${binary_path}/${_binary_name}.*\s+cap_dac_read_search'",
    }
  } else {
    file { "${binary_path}/${_binary_name}" :
      ensure  => file,
      owner   => 0,
      group   => 0,
      mode    => '0750',
      require => [
        Archive[$_downloaded_filename],
        Exec["ln ${binary_path}/${_downloaded_filename} ${binary_path}/${_binary_name}"],
      ],
    }
  }

  # setup logrotate stuffs
  file { $log_path :
    ensure => directory,
    owner  => $runasuser,
    group  => $runasuser,
    mode   => '0750',
  }

  if $logrotate {
    file { '/etc/logrotate.d/restic':
      ensure  => file,
      content => epp('restic/logrotate.d.conf.epp', { 'log_path' => $log_path }),
      owner   => 0,
      group   => 0,
      mode    => '0644',
    }
  } else {
    file { '/etc/logrotate.d/restic':
      ensure  => absent,
    }
  }
}
